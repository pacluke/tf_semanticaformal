(*
=====*=====*=====*=====*=====*=====

	UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
	INSTITUTO DE INFORMÁTICA
	DEPARTAMENTO DE INFORMÁTICA TEÓRICA
	INF05516 - Semântica Formal - Turma U
	Prof. Dr. Alvaro Moreira

	Trabalho Final - 2017/1
	28 de Junho de 2017

	Nome: Lucas Flores		Matrícula: 242317
	Nome: Raphael Piegas	Matrícula: 217430
	Nome: Taiane Peixoto	Matrícula: 228369

=====*=====*=====*=====*=====*=====

	DESCRIÇÃO:
	O trabalho consiste da implementação de um interpretador composto de um avaliador de
	expressões e de uma inferência de tipos para a linguagem L1 cuja sintaxe abstrata é dada
	abaixo. Tanto o avaliador de expressões como a inferência de tipos devem seguir estritamente
	a semântica operacional e o sistema de tipos definidos para L1. O avaliador de expressões
	deve ser implementado seguindo as regras da semântica operacional dadas no estilo big step.

	e ∈ Terms
	e ::= n
		| b
		| e1 op e2
		| if e1 then e2 else e3
		| x
		| e1 e2
		| fn x:T ⇒ e
		| let x:T = e1 in e2
		| let rec f:T1 → T2 = (fn y:T1 ⇒ e1) in e2

	onde
		n ∈ conjunto de numerais inteiros
		b ∈ {true, false}
		x ∈ Ident
		op ∈ {+, −, ∗, div, ==, and, or, not}

*)


	type variable = string;;

	type operator =
		Sub
		| Or
		| Sum
		| Div
		| And
		| Mult
		| Less
		| Equal
		| Greater
		| NotEqual
		| LessOrEqual
		| GreaterOrEqual
	;;

	type tipo =
		TyInt
		| TyBool
		| TyFn of tipo * tipo
	;;

	type expr =
		Num of int
		| Bool of bool
		| Bop of expr * operator * expr
		| If of expr * expr * expr
		| Var of variable
		| App of expr * expr
		| Func of variable * tipo * expr
		| Let of variable * tipo * expr * expr
		| LetRec of variable * (tipo * tipo) * (variable * tipo * expr) * expr
		| Not of expr
	;;

	type value =
		Vnum of int
		| Vbool of bool
		| Vclos of variable * expr * env
		| Vrclos of variable * variable * expr * env
	and
		env = (variable * value) list
	;;

	(* ==*==*==* SSM2 *==*==*== *)

	type instruction =
		INT of int
		| BOOL of bool
		| POP
		| COPY
		| ADD
		| INV
		| EQ 
		| GT 
		| AND
		| OR
		| NOT
		| JUMP of int
		| JUMPIFTRUE of int
		| VAR of variable
		| FUN of variable * code
		| RFUN of variable * variable * code
		| APPLY
	and
		code = instruction list
	;;

	type storableValue =
		SVint of int
		| SVbool of bool
		| SVclos of env * variable * code
		| SVrclos of env * variable * variable * code
	;;

	

(*
=====*=====*=====*=====*=====*=====
	CONTROLE DO AMBIENTE
=====*=====*=====*=====*=====*=====
*)

(* ==*== Atualizar o ambiente ==*== *)

	let updateEnv variable v environment : env = match environment with
		|[] -> [(variable, v)]
		| hd::tl -> List.append [(variable, v)] environment

(* ==*== Busca no ambiente ==*== *)

	let rec searchEnv variable environment : value = match environment with
		| [] -> raise Not_found
		| (k, v)::tl ->
			if (k = variable)
			then v
			else searchEnv variable tl

(* ==*== Ambiente vazio (usamos nos testes) ==*== *)

	let emptyEnv : env = []

(*
=====*=====*=====*=====*=====*=====
	SSM2: COMPILAÇÃO DE L1
=====*=====*=====*=====*=====*=====
*)

	let rec compiler environment expr : code = match expr with
		| Num(n) -> [INT(n)]
		| Bool(b) -> [BOOL(b)]

		| Bop(e1, op, e2) -> 
			let exp1 = compiler environment e1 in
			let exp2 = compiler environment e2 in
			(match op with
				| Sub -> List.append (List.append exp2 (List.append [INV] exp1)) [ADD]
				| Or -> List.append exp2 (List.append exp1 [OR])
				| Sum -> List.append exp2 (List.append exp1 [ADD])
				| And -> List.append exp2 (List.append exp1 [AND])
				| Equal -> List.append exp2 (List.append exp1 [EQ])
				| Greater -> List.append exp2 (List.append exp1 [GT])

				(* ==*== caso de falha ==*== *)
				| _ -> failwith "bop_not_found"
			)
		| If(e1, e2, e3) -> 
			let exp1 = compiler environment e1 in
			let exp2 = compiler environment e2 in
			let exp3 = compiler environment e3 in
			let lengthE2 = List.length exp2 in
			let lengthE3 = List.length exp2 in (
				List.append
					(List.append
						(List.append
							exp1
							(List.append
								[JUMPIFTRUE(lengthE3 + 1)]
								exp3))
						[JUMP lengthE2])
					exp2
			)

		| Var(x) -> [VAR(x)]
		| App(e1, e2) -> 
			let exp1 = compiler environment e1 in
			let exp2 = compiler environment e2 in (
				List.append exp2 (List.append exp1 [APPLY])
			)

		| Func(variable, t, e) -> [FUN(variable, (compiler environment e))]

		| Let(variable, t, e1, e2) -> 
			let exp1 = compiler environment e1 in
			let exp2 = compiler environment e2 in (
				List.append exp1 (List.append [FUN(variable, exp2)] [APPLY])
			)

		| LetRec(f, (t1, t2), (var, t3, e1), e2) ->
			let exp1 = compiler environment e1 in
			let exp2 = compiler environment e2 in (
				[RFUN(f, var, exp1); FUN(f, exp2); APPLY]
			)

		| Not(e1) -> List.append (compiler environment e1) [NOT]
	;;

	























