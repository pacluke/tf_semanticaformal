(*
=====*=====*=====*=====*=====*=====

	UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
	INSTITUTO DE INFORMÁTICA
	DEPARTAMENTO DE INFORMÁTICA TEÓRICA
	INF05516 - Semântica Formal - Turma U
	Prof. Dr. Alvaro Moreira

	Trabalho Final - 2017/1
	28 de Junho de 2017

	Nome: Lucas Flores		Matrícula: 242317
	Nome: Rafael Piegas		Matrícula: 217430
	Nome: Taiane Peixoto	Matrícula: 228369

=====*=====*=====*=====*=====*=====

	DESCRIÇÃO:
	O trabalho consiste da implementação de um interpretador composto de um avaliador de
	expressões e de uma inferência de tipos para a linguagem L1 cuja sintaxe abstrata é dada
	abaixo. Tanto o avaliador de expressões como a inferência de tipos devem seguir estritamente
	a semântica operacional e o sistema de tipos definidos para L1. O avaliador de expressões
	deve ser implementado seguindo as regras da semântica operacional dadas no estilo big step.

	e ∈ Terms
	e ::= n
		| b
		| e1 op e2
		| if e1 then e2 else e3
		| x
		| e1 e2
		| fn x:T ⇒ e
		| let x:T = e1 in e2
		| let rec f:T1 → T2 = (fn y:T1 ⇒ e1) in e2

	onde
		n ∈ conjunto de numerais inteiros
		b ∈ {true, false}
		x ∈ Ident
		op ∈ {+, −, ∗, div, ==, and, or, not}

*)


	type variable = string;;

	type operator =
		Sub
		| Or
		| Sum
		| Div
		| And
		| Mult
		| Less
		| Equal
		| Greater
		| NotEqual
		| LessOrEqual
		| GreaterOrEqual
	;;

	type tipo =
		TyInt
		| TyBool
		| TyFn of tipo * tipo
	;;

	type expr =
		Num of int
		| Bool of bool
		| Bop of expr * operator * expr
		| If of expr * expr * expr
		| Var of variable
		| App of expr * expr
		| Func of variable * tipo * expr
		| Let of variable * tipo * expr * expr
		| LetRec of variable * (tipo * tipo) * (variable * tipo * expr) * expr
		| Not of expr
	;;

	type value =
		Vnum of int
		| Vbool of bool
		| Vclos of variable * expr * env
		| Vrclos of variable * variable * expr * env
	and
		env = (variable * value) list
	;;


(*
=====*=====*=====*=====*=====*=====
	CONTROLE DO AMBIENTE
=====*=====*=====*=====*=====*=====
*)

(* ==*== Atualizar o ambiente ==*== *)

	let updateEnv variable v environment : env = match environment with
		|[] -> [(variable, v)]
		| hd::tl -> List.append [(variable, v)] environment

(* ==*== Busca no ambiente ==*== *)

	let rec searchEnv variable environment : value = match environment with
		| [] -> raise Not_found
		| (k, v)::tl ->
			if (k = variable)
			then v
			else searchEnv variable tl

(* ==*== Ambiente vazio (usamos nos testes) ==*== *)

	let emptyEnv : env = []

(*
=====*=====*=====*=====*=====*=====
	AVALIAÇÃO - BIG STEP
=====*=====*=====*=====*=====*=====
*)

	let rec eval environment e : value = match e with

		(* ==*== Valores ==*== *)
		Num(n) -> Vnum(n)
		| Bool(b) -> Vbool(b)

		(* ==*== Operações binárias ==*== *)
		| Bop(e1, op, e2) -> 
			let exp1 = eval environment e1 in
			let exp2 = eval environment e2 in
			(match exp1, op, exp2 with

				(* ==*== retorna tipo inteiro ==*== *)
				Vnum(exp1), Sum, Vnum(exp2) -> Vnum(exp1 + exp2)
				|Vnum(exp1), Sub, Vnum(exp2) -> Vnum(exp1 - exp2)
				|Vnum(exp1), Div, Vnum(exp2) -> Vnum(exp1 / exp2)
				|Vnum(exp1), Mult, Vnum(exp2) -> Vnum(exp1 * exp2)

				(* ==*== retorna tipo booleano ==*== *)
				|Vnum(exp1), Equal, Vnum(exp2) -> Vbool(exp1 = exp2)
				|Vbool(exp1), Equal, Vbool(exp2) -> Vbool(exp1 = exp2)
				|Vnum(exp1), NotEqual, Vnum(exp2) -> Vbool(exp1 <> exp2)
				|Vbool(exp1), NotEqual, Vbool(exp2) -> Vbool(exp1 <> exp2)
				|Vnum(exp1), Less, Vnum(exp2) -> Vbool(exp1 < exp2)
				|Vnum(exp1), LessOrEqual, Vnum(exp2) -> Vbool(exp1 <= exp2)
				|Vnum(exp1), Greater, Vnum(exp2) -> Vbool(exp1 > exp2)
				|Vnum(exp1), GreaterOrEqual, Vnum(exp2) -> Vbool(exp1 >= exp2)
				|Vbool(exp1), And, Vbool(exp2) -> Vbool(exp1 && exp2)
				|Vbool(exp1), Or, Vbool(exp2) -> Vbool(exp1 || exp2)

				(* ==*== caso de falha ==*== *)
				| _ -> failwith "bop_not_found"
			)

		(* ==*== if then else ==*== *)
		| If(e1, e2, e3) when ((eval environment e1) = Vbool(true)) -> eval environment e2
		| If(e1, e2, e3) when ((eval environment e1) = Vbool(false)) -> eval environment e3

		(* ==*== Variavel ==*== *)
		| Var(variable) -> searchEnv variable environment

		(* ==*== Função ==*== *)
		| Func(variable, t, e) -> Vclos(variable, e, environment)

		(* ==*== Aplicação ==*== *)
		| App(e1, e2) -> 
			let exp3 = eval environment e1 in
			let exp4 = eval environment e2 in
			(match exp3, exp4 with
				Vclos(var, e, envi), v -> eval (updateEnv var v envi) e
				| Vrclos(f, x, e, envi), v -> eval (updateEnv f (Vrclos(f, x, e, envi)) (updateEnv x v envi)) e

				(* ==*== caso de falha ==*== *)
				| _ -> failwith "app_not_found"
			)

		(* ==*== Let ==*== *)
		| Let(var, t, e1, e2) ->
			let exp5 = eval environment e1 in
			eval (updateEnv var exp5 environment) e2

		(* ==*== Let Rec ==*== *)
		| LetRec(f, (t1, t2), (var, t3, e1), e2) ->
			let exp6 = eval environment (Func(var, t3, e1)) in
			(match exp6 with
				| Vclos(x, e, env) -> eval (updateEnv f (Vrclos(f, x, e, environment)) env) e2

				(* ==*== caso de falha ==*== *)
				| _ -> failwith "letrec_not_found"
			)

		(* ==*== Not ==*== *)
		| Not(e) when ((eval environment e) = Vbool(true)) -> Vbool(false)
		| Not(e) when ((eval environment e) = Vbool(false)) -> Vbool(true)

		(* ==*== caso de falha ==*== *)
		| _ -> failwith "eval_not_found"
	;;



(*
=====*=====*=====*=====*=====*=====
	TESTES
=====*=====*=====*=====*=====*=====
*)


	(* ==*== Ambiente vazio ==*== *)
	let environment =  emptyEnv;;
	
	(* ==*== Valores basicos ==*== *)
	let numTest = Vnum (5);;
	let boolTest = Vbool (true);;

	(* ==*== Estrutura do ambiente ==*== *)
	let envir = updateEnv "numTest" numTest environment;;
	searchEnv "numTest" envir;;
	let envir' = updateEnv "ten" (Vnum(10)) envir;;
	let envir''= updateEnv "numTest" (Vnum(6)) envir';;
	searchEnv "numTest" envir;;


	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let notOK = Not(Bool(true));;
	let notFail = Not(Num(1));;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let bopSubOK = Bop(Num(10), Sub, Num(5));;
	let bopSubFail = Bop(Bool(false), Sub, Num(5));;

	(*	| Or 	*)
	let bopOrOK = Bop(Bool(false), Or, Bool(true));;
	let bopOrFail = Bop(Bool(false), Or, Num(5));;

	(*	| Sum 	*)
	let bopSumOK = Bop(Num(10), Sum, Num(5));;
	let bopSumFail = Bop(Bool(false), Sum, Num(5));;

	(*	| Div 	*)
	let bopDivOK = Bop(Num(10), Div, Num(5));;
	let bopDivFail = Bop(Bool(false), Div, Num(5));;

	(*	| And 	*)
	let bopAndOK = Bop(Bool(false), And, Bool(true));;
	let bopAndFail = Bop(Bool(false), And, Num(5));;

	(*	| Mult 	*)
	let bopMultOK = Bop(Num(10), Mult, Num(5));;
	let bopMultFail = Bop(Bool(false), Mult, Num(5));;

	(*	| Less 	*)
	let bopLessOK = Bop(Num(10), Less, Num(5));;
	let bopLessFail = Bop(Bool(false), Less, Num(5));;

	(*	| Equal 	*)
	let bopEqNumOK = Bop(Num(10), Equal, Num(5));;
	let bopEqNumFail = Bop(Bool(false), Equal, Num(5));;

	let bopEqBoolOK = Bop(Bool(false), Equal, Bool(true));;
	let bopEqBoolFail = Bop(Bool(false), Equal, Num(5));;

	(*	| Greater 	*)
	let bopGreaterOK = Bop(Num(10), Greater, Num(5));;
	let bopGreaterFail = Bop(Bool(false), Greater, Num(5));;

	(*	| NotEqual 	*)
	let bopNotEqNumOK = Bop(Num(10), NotEqual, Num(5));;
	let bopNotEqNumFail = Bop(Bool(false), NotEqual, Num(5));;

	let bopNotEqBoolOK = Bop(Bool(false), NotEqual, Bool(true));;
	let bopNotEqBoolFail = Bop(Bool(false), NotEqual, Num(5));;

	(*	| LessOrEqual 	*)
	let bopLessOrEqualOK = Bop(Num(10), LessOrEqual, Num(5));;
	let bopLessOrEqualFail = Bop(Bool(false), LessOrEqual, Num(5));;

	(*	| GreaterOrEqual 	*)
	let bopGreaterOrEqualOK = Bop(Num(10), GreaterOrEqual, Num(5));;
	let bopGreaterOrEqualFail = Bop(Bool(false), GreaterOrEqual, Num(5));;


	(* ==*== IF ==*== *)
	let ifTrue = If(Bop(Num(99), NotEqual, Num(25)), Bool(true), Bool(false));;
	let ifFalse = If(Bop(Num(99), NotEqual, Num(25)), Bool(true), Bool(false));;
	let ifFail = If(Bop(Num(99), Sub, Num(25)), Num(44), Num(72));;


	(* ==*== FUNCTION ==*== *)
	let funcOk = Func("x", TyInt, Bop(Var("x"), Sub, Num(1)));;


	(* ==*== APLICAÇÃO ==*== *)
	let appVar = Var("x");;
	let appOK = App(Func("x", TyInt, Bop(appVar, Sub, Num(4))), Num(5));;


	(* ==*== LET ==*== *)
	let letOK01 = Let("x", TyInt, Num(2), Bop(Var("x"), Sub, Num(2)));;
	let letOK02 = Let("x", TyInt, Num(2), Let("y", TyInt, Num(2), Bop(Var("x"), Mult, Var("y"))));;
	let letOK03 = Let("x", TyInt, Num(111), If(Bop(Var("x"), Equal, Num(111)), Bool(true), Bool(false)));;


	(* ==*== LET REC ==*== *)
		(* ==*== EX. FATORIAL - FAT(10) ==*== *)
	let letRecOK01 = LetRec("fat", (TyInt, TyInt), ("x", TyInt,
							If(Bop(Var("x"), Equal, Num(0)),
								Num(1),
								Bop(Var("x"), Mult, App(Var("fat"), Bop(Var("x"), Sub, Num(1))))
							)),
							App(Var("fat"), Num(10))
						);;

		(* ==*== EX. FIBONACCI - FIB(10)  ==*== *)
	let letRecOK02 = LetRec("fib", (TyInt, TyInt), ("x", TyInt,
							If(Bop(Var("x"), Less, Num(2)),
								Var("x"),
								Bop(App(Var("fib"), Bop(Var("x"), Sub, Num(1))), Sum, App(Var("fib"), Bop(Var("x"), Sub, Num(2))))
							)),
							App(Var("fib"), Num(10))
						);;


(*
=====*=====*=====*=====*=====*=====
	EVAL: TESTES QUE DEVEM PASSAR
=====*=====*=====*=====*=====*=====
*)
	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let testNotOK = eval environment notOK;;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let testBopSubOK = eval environment bopSubOK;;

	(*	| Or 	*)
	let testBopOrOK = eval environment bopOrOK;;

	(*	| Sum 	*)
	let testBopSumOK = eval environment bopSumOK;;

	(*	| Div 	*)
	let testBopDivOK = eval environment bopDivOK;;

	(*	| And 	*)
	let testBopAndOK = eval environment bopAndOK;;

	(*	| Mult 	*)
	let testBopMultOK = eval environment bopMultOK;;

	(*	| Less 	*)
	let testBopLessOK = eval environment bopLessOK;;

	(*	| Equal 	*)
	let testBopEqNumOK = eval environment bopEqNumOK;;

	let testBopEqBoolOK = eval environment bopEqBoolOK;;

	(*	| Greater 	*)
	let testBopGreaterOK = eval environment bopGreaterOK;;

	(*	| NotEqual 	*)
	let testBopNotEqNumOK = eval environment bopNotEqNumOK;;

	let testBopNotEqBoolOK = eval environment bopNotEqBoolOK;;

	(*	| LessOrEqual 	*)
	let testBopLessOrEqualOK = eval environment bopLessOrEqualOK;;

	(*	| GreaterOrEqual 	*)
	let testBopGreaterOrEqualOK = eval environment bopGreaterOrEqualOK;;


	(* ==*== IF ==*== *)
	let testIfFalse = eval environment ifFalse;;
	let testIfTrue = eval environment ifTrue;;


	(* ==*== FUNCTION ==*== *)
	let testFuncOK = eval environment funcOk;;


	(* ==*== APLICAÇÃO ==*== *)
	let testAppOk = eval environment appOK;;


	(* ==*== LET ==*== *)
	let testLetOK01 = eval environment letOK01;;
	let testLetOK02 = eval environment letOK02;;
	let testLetOK03 = eval environment letOK03;;


	(* ==*== LET REC ==*== *)
		(* ==*== EX. FATORIAL - FAT(10) ==*== *)
	let testLetRecOK01 = eval environment letRecOK01;;

		(* ==*== EX. FIBONACCI - FIB(10)  ==*== *)
	let testLetRecOK02 = eval environment letRecOK02;;


(*
=====*=====*=====*=====*=====*=====
	EVAL: TESTES QUE DEVEM DAR FAIL
=====*=====*=====*=====*=====*=====


	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let testNotFail = eval environment notFail;;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let testBopSubFail = eval environment bopSubFail;;

	(*	| Or 	*)
	let testBopOrFail = eval environment bopOrFail;;

	(*	| Sum 	*)
	let testBopSumFail = eval environment bopSumFail;;

	(*	| Div 	*)
	let testBopDivFail = eval environment bopDivFail;;

	(*	| And 	*)
	let testBopAndFail = eval environment bopAndFail;;

	(*	| Mult 	*)
	let testBopMultFail = eval environment bopMultFail;;

	(*	| Less 	*)
	let testBopLessFail = eval environment bopLessFail;;

	(*	| Equal 	*)
	let testBopEqNumFail = eval environment bopEqNumFail;;

	let testBopEqBoolFail = eval environment bopEqBoolFail;;

	(*	| Greater 	*)
	let testBopGreaterFail = eval environment bopGreaterFail;;

	(*	| NotEqual 	*)
	let testBopNotEqNumFail = eval environment bopNotEqNumFail;;

	let testBopNotEqBoolFail = eval environment bopNotEqBoolFail;;

	(*	| LessOrEqual 	*)
	let testBopLessOrEqualFail = eval environment bopLessOrEqualFail;;

	(*	| GreaterOrEqual 	*)
	let testBopGreaterOrEqualFail = eval environment bopGreaterOrEqualFail;;


	(* ==*== IF ==*== *)
	let testIfFail = eval environment ifFalse;;

*)