(*
=====*=====*=====*=====*=====*==========*=====*=====*=====*=====*==========*=====*=====*=====*=====
	UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
	INSTITUTO DE INFORMÁTICA
	DEPARTAMENTO DE INFORMÁTICA TEÓRICA
	INF05516 - Semântica Formal - Turma U
	Prof. Dr. Alvaro Moreira

	Trabalho Final - 2017/1
	28 de Junho de 2017

	Nome: Lucas Flores		Matrícula: 242317
	Nome: Rafael Piegas		Matrícula: 217430
	Nome: Taiane Peixoto	Matrícula: 228369

=====*=====*=====*=====*=====*==========*=====*=====*=====*=====*==========*=====*=====*=====*=====

	DESCRIÇÃO:
	O trabalho consiste da implementação de um interpretador composto de um avaliador de
	expressões e de uma inferência de tipos para a linguagem L1 cuja sintaxe abstrata é dada
	abaixo. Tanto o avaliador de expressões como a inferência de tipos devem seguir estritamente
	a semântica operacional e o sistema de tipos definidos para L1. O avaliador de expressões
	deve ser implementado seguindo as regras da semântica operacional dadas no estilo big step.

	e ∈ Terms
	e ::= n
		| b
		| e1 op e2
		| if e1 then e2 else e3
		| x
		| e1 e2
		| fn x:T ⇒ e
		| let x:T = e1 in e2
		| let rec f:T1 → T2 = (fn y:T1 ⇒ e1) in e2

	onde
		n ∈ conjunto de numerais inteiros
		b ∈ {true, false}
		x ∈ Ident
		op ∈ {+, −, ∗, div, ==, and, or, not}
*)

	type variable = string;;

	type operator =
		Sub
		| Or
		| Sum
		| Div
		| And
		| Mult
		| Less
		| Equal
		| Greater
		| NotEqual
		| LessOrEqual
		| GreaterOrEqual
	;;

	type tipo =
		TyInt
		| TyBool
		| TyFn of tipo * tipo
	and
		tenv = (variable * tipo) list
	;;

	type expr =
		Num of int
		| Bool of bool
		| Bop of expr * operator * expr
		| If of expr * expr * expr
		| Var of variable
		| App of expr * expr
		| Func of variable * tipo * expr
		| Let of variable * tipo * expr * expr
		| LetRec of variable * (tipo * tipo) * (variable * tipo * expr) * expr
		| Not of expr
	;;

	type value =
		Vnum of int
		| Vbool of bool
		| Vclos of variable * expr * env
		| Vrclos of variable * variable * expr * env
	and
		env = (variable * value) list
	;;


	
(*
=====*=====*=====*=====*=====*=====
	CONTROLE DO AMBIENTE
=====*=====*=====*=====*=====*=====
*)

(* ==*== Atualizar o ambiente ==*== *)

	let updateEnv variable tipo environment : tenv = match environment with
		|[] -> [(variable, tipo)]
		| hd::tl -> List.append [(variable, tipo)] environment

(* ==*== Busca no ambiente ==*== *)

	let rec searchEnv variable environment : tipo = match environment with
		| [] -> raise Not_found
		| (k, v)::tl ->
			if (k = variable)
			then v
			else searchEnv variable tl

(* ==*== Ambiente vazio (usamos nos testes) ==*== *)

	let emptyEnv : tenv = []

(*
=====*=====*=====*=====*=====*=====
	INFERÊNCIA DE TIPOS
=====*=====*=====*=====*=====*=====
*)


let rec typecheck (environment:tenv) (e:expr) : tipo = 
	match e with
	(* VALORES *)
	  Num(e) -> TyInt
	| Bool(e) -> TyBool

	(* OPERAÇÕES BINÁRIAS *)
	| Bop(e1, op, e2) ->
		let exp1 = typecheck environment e1 in
		let exp2 = typecheck environment e2 in
		(match exp1, op, exp2 with

			(* Retorna Tipo Int *)
			  TyInt, Sum, TyInt -> TyInt
			| TyInt, Sub, TyInt -> TyInt
			| TyInt, Mult, TyInt -> TyInt
			| TyInt, Div, TyInt -> TyInt
			(* Retorna Tipo Bool *)
			| TyInt, Equal, TyInt -> TyBool
			| TyBool, Equal, TyBool -> TyBool
			| TyBool, NotEqual, TyBool -> TyBool
			| TyInt, NotEqual, TyInt -> TyBool
			| TyInt, GreaterOrEqual, TyInt -> TyBool
			| TyBool, And, TyBool -> TyBool
			| TyBool, Or, TyBool -> TyBool
			| TyInt, Less, TyInt -> TyBool
			| TyInt, LessOrEqual, TyInt -> TyBool
			| TyInt, Greater, TyInt -> TyBool
			| _ -> failwith "ERROR: 'BOP' NOT FOUND"
		)


	(* NOT *)
	| Not(e) -> 
		(match e with 
			  Bool(e) -> TyBool
			| _ ->failwith "ERROR: 'NOT' NOT FOUND"
		)


	(* CONDICIONAL *)
	| If(e1, e2, e3) ->
		if (typecheck environment e1) = TyBool
		then 
			let t1 = typecheck environment e2 in
			let t2 = typecheck environment e3 in
			(	if t1 = t2
				then t1
				else failwith "ERROR: DIFFRENT TYPES"
			) 
		else failwith "ERROR: 'IF' NOT FOUND"


	(* VARIÁVEL *)
	| Var(variable) -> searchEnv variable environment


	(* APLICAÇÃO *)
	| App(e1, e2) ->
		let expapp1 = typecheck environment e1 in
		let expapp2 = typecheck environment e2 in
		(match expapp1 with
			TyFn(t1, t2) -> (if t2 = expapp2
							then expapp2
							else failwith "ERROR: 'APP' NOT FOUND")
			| _ -> failwith "ERROR: 'APP' NOT FOUND"

		)
					
	(* FUNÇÃO *)
	| Func(variable, t, e) -> TyFn(t, (typecheck (updateEnv variable t environment) e))
	
	(* LET *)
	| Let(variable, t, e1, e2) ->
		let v = typecheck environment e1 in (
			if v = t
			then typecheck (updateEnv variable t environment) e2
			else failwith "ERROR: 'APP' NOT FOUND")

	(* LET REC *)
	| LetRec(f, (t1, t2), (var, t3, e1), e2) ->
		let t5 = TyFn(t1, t2) in
		let update1 = updateEnv var t3 environment in
		let update2 = updateEnv f t5 update1 in
		let t4 = typecheck update2 e1 in (
			if t4 = t2 && t1 = t3
			then typecheck (updateEnv f t5 environment) e2
			else failwith "ERROR: 'LET REC' NOT FOUND"
		)

		(* *)
	
	
	;;
	 
(*
=====*=====*=====*=====*=====*=====
	TESTES
=====*=====*=====*=====*=====*=====
*)


	(* ==*== Ambiente vazio ==*== *)
 	let environment = emptyEnv;;
	
	(* ==*== Valores basicos ==*== *)
	let numTest = Vnum (5);;
	let boolTest = Vbool (true);;

	(* ==*== Estrutura do ambiente ==*== *)
	let envir = updateEnv "numTest" TyInt environment;;
	searchEnv "numTest" envir;;
	let envir' = updateEnv "ten" TyInt envir;;
	let envir''= updateEnv "numTest" TyInt envir';;
	searchEnv "numTest" envir;;


	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let notOK = Not(Bool(true));;
	let notFail = Not(Num(1));;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let bopSubOK = Bop(Num(10), Sub, Num(5));;
	let bopSubFail = Bop(Bool(false), Sub, Num(5));;

	(*	| Or 	*)
	let bopOrOK = Bop(Bool(false), Or, Bool(true));;
	let bopOrFail = Bop(Bool(false), Or, Num(5));;

	(*	| Sum 	*)
	let bopSumOK = Bop(Num(10), Sum, Num(5));;
	let bopSumFail = Bop(Bool(false), Sum, Num(5));;

	(*	| Div 	*)
	let bopDivOK = Bop(Num(10), Div, Num(5));;
	let bopDivFail = Bop(Bool(false), Div, Num(5));;

	(*	| And 	*)
	let bopAndOK = Bop(Bool(false), And, Bool(true));;
	let bopAndFail = Bop(Bool(false), And, Num(5));;

	(*	| Mult 	*)
	let bopMultOK = Bop(Num(10), Mult, Num(5));;
	let bopMultFail = Bop(Bool(false), Mult, Num(5));;

	(*	| Less 	*)
	let bopLessOK = Bop(Num(10), Less, Num(5));;
	let bopLessFail = Bop(Bool(false), Less, Num(5));;

	(*	| Equal 	*)
	let bopEqNumOK = Bop(Num(10), Equal, Num(5));;
	let bopEqNumFail = Bop(Bool(false), Equal, Num(5));;

	let bopEqBoolOK = Bop(Bool(false), Equal, Bool(true));;
	let bopEqBoolFail = Bop(Bool(false), Equal, Num(5));;

	(*	| Greater 	*)
	let bopGreaterOK = Bop(Num(10), Greater, Num(5));;
	let bopGreaterFail = Bop(Bool(false), Greater, Num(5));;

	(*	| NotEqual 	*)
	let bopNotEqNumOK = Bop(Num(10), NotEqual, Num(5));;
	let bopNotEqNumFail = Bop(Bool(false), NotEqual, Num(5));;

	let bopNotEqBoolOK = Bop(Bool(false), NotEqual, Bool(true));;
	let bopNotEqBoolFail = Bop(Bool(false), NotEqual, Num(5));;

	(*	| LessOrEqual 	*)
	let bopLessOrEqualOK = Bop(Num(10), LessOrEqual, Num(5));;
	let bopLessOrEqualFail = Bop(Bool(false), LessOrEqual, Num(5));;

	(*	| GreaterOrEqual 	*)
	let bopGreaterOrEqualOK = Bop(Num(10), GreaterOrEqual, Num(5));;
	let bopGreaterOrEqualFail = Bop(Bool(false), GreaterOrEqual, Num(5));;


	(* ==*== IF ==*== *)
	let ifTrue = If(Bop(Num(99), NotEqual, Num(25)), Bool(true), Bool(false));;
	let ifFalse = If(Bop(Num(99), NotEqual, Num(25)), Bool(true), Bool(false));;
	let ifFail = If(Bop(Num(99), Sub, Num(25)), Num(44), Num(72));;


	(* ==*== FUNCTION ==*== *)
	let funcOk = Func("x", TyInt, Bop(Var("x"), Sub, Num(1)));;


	(* ==*== APLICAÇÃO ==*== *)
	let appVar = Var("x");;
	let appOK = App(Func("x", TyInt, Bop(appVar, Sub, Num(4))), Num(5));;


	(* ==*== LET ==*== *)
	let letOK01 = Let("x", TyInt, Num(2), Bop(Var("x"), Sub, Num(2)));;
	let letOK02 = Let("x", TyInt, Num(2), Let("y", TyInt, Num(2), Bop(Var("x"), Mult, Var("y"))));;
	let letOK03 = Let("x", TyInt, Num(111), If(Bop(Var("x"), Equal, Num(111)), Bool(true), Bool(false)));;


	(* ==*== LET REC ==*== *)
		(* ==*== EX. FATORIAL - FAT(10) ==*== *)
	let letRecOK01 = LetRec("fat", (TyInt, TyInt), ("x", TyInt,
							If(Bop(Var("x"), Equal, Num(0)),
								Num(1),
								Bop(Var("x"), Mult, App(Var("fat"), Bop(Var("x"), Sub, Num(1))))
							)),
							App(Var("fat"), Num(10))
						);;

		(* ==*== EX. FIBONACCI - FIB(10)  ==*== *)
	let letRecOK02 = LetRec("fib", (TyInt, TyInt), ("x", TyInt,
							If(Bop(Var("x"), Less, Num(2)),
								Var("x"),
								Bop(App(Var("fib"), Bop(Var("x"), Sub, Num(1))), Sum, App(Var("fib"), Bop(Var("x"), Sub, Num(2))))
							)),
							App(Var("fib"), Num(10))
						);;



(*
=====*=====*=====*=====*=====*=====
	TYPECHECK: TESTES QUE DEVEM PASSAR
=====*=====*=====*=====*=====*=====
*)
	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let testNotOK = typecheck environment notOK;;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let testBopSubOK = typecheck environment bopSubOK;;

	(*	| Or 	*)
	let testBopOrOK = typecheck environment bopOrOK;;

	(*	| Sum 	*)
	let testBopSumOK = typecheck environment bopSumOK;;

	(*	| Div 	*)
	let testBopDivOK = typecheck environment bopDivOK;;

	(*	| And 	*)
	let testBopAndOK = typecheck environment bopAndOK;;

	(*	| Mult 	*)
	let testBopMultOK = typecheck environment bopMultOK;;

	(*	| Less 	*)
	let testBopLessOK = typecheck environment bopLessOK;;

	(*	| Equal 	*)
	let testBopEqNumOK = typecheck environment bopEqNumOK;;

	let testBopEqBoolOK = typecheck environment bopEqBoolOK;;

	(*	| Greater 	*)
	let testBopGreaterOK = typecheck environment bopGreaterOK;;

	(*	| NotEqual 	*)
	let testBopNotEqNumOK = typecheck environment bopNotEqNumOK;;

	let testBopNotEqBoolOK = typecheck environment bopNotEqBoolOK;;

	(*	| LessOrEqual 	*)
	let testBopLessOrEqualOK = typecheck environment bopLessOrEqualOK;;

	(*	| GreaterOrEqual 	*)
	let testBopGreaterOrEqualOK = typecheck environment bopGreaterOrEqualOK;;


	(* ==*== IF ==*== *)
	let testIfFalse = typecheck environment ifFalse;;
	let testIfTrue = typecheck environment ifTrue;;


	(* ==*== FUNCTION ==*== *)

	let testFuncOK = typecheck (environment) funcOk;;


	(* ==*== APLICAÇÃO ==*== *)
	let testAppOk = typecheck environment appOK;;


	(* ==*== LET ==*== *)
	let testLetOK01 = typecheck environment letOK01;;
	let testLetOK02 = typecheck environment letOK02;;
	let testLetOK03 = typecheck environment letOK03;;


	(* ==*== LET REC ==*== *)
		(* ==*== EX. FATORIAL - FAT(10) ==*== *)
	let testLetRecOK01 = typecheck environment letRecOK01;;

		(* ==*== EX. FIBONACCI - FIB(10)  ==*== *)
	let testLetRecOK02 = typecheck environment letRecOK02;;


(*
=====*=====*=====*=====*=====*=====
	TYPECHECK: TESTES QUE DEVEM DAR FAIL
=====*=====*=====*=====*=====*=====


	(* ==*== EXPRESSÕES ==*== *)

	(* ==*== Not ==*== *)
	let testNotFail = typecheck environment notFail;;


	(* ==*== Bop ==*== *)
	(* 	Sub 	*)
	let testBopSubFail = typecheck environment bopSubFail;;

	(*	| Or 	*)
	let testBopOrFail = typecheck environment bopOrFail;;

	(*	| Sum 	*)
	let testBopSumFail = typecheck environment bopSumFail;;

	(*	| Div 	*)
	let testBopDivFail = typecheck environment bopDivFail;;

	(*	| And 	*)
	let testBopAndFail = typecheck environment bopAndFail;;

	(*	| Mult 	*)
	let testBopMultFail = typecheck environment bopMultFail;;

	(*	| Less 	*)
	let testBopLessFail = typecheck environment bopLessFail;;

	(*	| Equal 	*)
	let testBopEqNumFail = typecheck environment bopEqNumFail;;

	let testBopEqBoolFail = typecheck environment bopEqBoolFail;;

	(*	| Greater 	*)
	let testBopGreaterFail = typecheck environment bopGreaterFail;;

	(*	| NotEqual 	*)
	let testBopNotEqNumFail = typecheck environment bopNotEqNumFail;;

	let testBopNotEqBoolFail = typecheck environment bopNotEqBoolFail;;

	(*	| LessOrEqual 	*)
	let testBopLessOrEqualFail = typecheck environment bopLessOrEqualFail;;

	(*	| GreaterOrEqual 	*)
	let testBopGreaterOrEqualFail = typecheck environment bopGreaterOrEqualFail;;


	(* ==*== IF ==*== *)
	let testIfFail = typecheck environment ifFalse;;

*)